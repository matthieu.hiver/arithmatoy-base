from tests.tp import addition, multiplication, nombre_entier


class Entier:
    n: str

    def __init__(self, n: str):
        if not Entier.is_valid(n):
            raise ValueError(f"Entier invalide : {n!r}")
        self.n = n

    def __str__(self):
        return self.n

    def __repr__(self):
        return f"Entier({self.n!r} | {int(self)!r})"

    def __add__(self, other: "Entier") -> "Entier":
        return Entier(addition(self.n, other.n))

    def __mul__(self, other: "Entier") -> "Entier":
        return Entier(multiplication(self.n, other.n))

    def __eq__(self, other: object) -> bool:
        return isinstance(other, Entier) and self.n == other.n

    def __int__(self) -> int:
        return len(self.n) - 1

    @classmethod
    def zero(cls) -> "Entier":
        return cls("0")

    @classmethod
    def from_int(cls, n: int) -> "Entier":
        if n < 0:
            raise ValueError(f"Entier négatif : {n!r}")
        return cls(nombre_entier(n))

    @classmethod
    def is_valid(cls, n: str) -> bool:
        return n.lstrip("S") == "0"
